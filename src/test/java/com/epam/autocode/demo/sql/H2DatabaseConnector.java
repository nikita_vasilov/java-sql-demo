package com.epam.autocode.demo.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class H2DatabaseConnector {

    private static final String JDBC_URL = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=false";
    private static final String JDBC_USERNAME = "sa";
    private static final String JDBC_PASSWORD = "";

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(JDBC_URL, JDBC_USERNAME, JDBC_PASSWORD);
    }

    public static SqlError getSqlError(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                var sqlException = (SQLException) e;
                return new SqlError(sqlException.getSQLState(), sqlException.getErrorCode(), sqlException.getMessage());
            }
        }
        return new SqlError("Unknown");
    }
}
