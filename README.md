### Java SQL demo repository for Autocode
* H2 in-memory database used for this example (can be replaced with SQLite for .NET)
* Students can write queries in the SqlQueries class covered with the unit test
* It may be possible for students to write their queries in .sql files and then the unit test will read them by some naming convention